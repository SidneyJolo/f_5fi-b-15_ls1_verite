import java.util.Scanner; // Import der Klasse Scanner
public class Rechner
	{
	public static void main(String[] args) // Hier startet das Programm
		{
			// Neues Scanner-Objekt myScanner wird erstellt
			Scanner myScanner = new Scanner(System.in);
			System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
			
			// Die Variable zahl1 speichert die erste Eingabe
			int zahl1 = myScanner.nextInt();
			System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
			
			// Die Variable zahl2 speichert die zweite Eingabe
			int zahl2 = myScanner.nextInt();
			
			// Die Addition der Variablen zahl1 und zahl2
			// wird der Variable ergebnis zugewiesen.
			double ergebnisAdd = zahl1 + zahl2;
			double ergebnisSub = zahl1 - zahl2;
			double ergebnisMul = zahl1 * zahl2;
			double ergebnisDiv = (double)zahl1 / (double)zahl2;
			
			System.out.print("\n\nErgebnis der Addition lautet: ");
			System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisAdd + "\n");
			System.out.print("\n\nErgebnis der Substraction lautet: ");
			System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisSub + "\n");
			System.out.print("\n\nErgebnis der Multiplication lautet: ");
			System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnisMul + "\n");
			System.out.print("\n\nErgebnis der Divition lautet: ");
			System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnisDiv + "\n");
			myScanner.close();
		}
}