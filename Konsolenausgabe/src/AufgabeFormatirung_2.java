
public class AufgabeFormatirung_2 {

	public static void main(String[] args) {
		//Aufgabe 1
		System.out.printf("\nAufgabe 1\n");
		
		String s = "*****";
		System.out.printf("%4.2s\n%.1s%5.1s\n%.1s%5.1s\n%4.2s\n", s,s,s,s,s,s);

		
		//Aufgabe2
		System.out.printf("\nAufgabe 2\n");
		
		System.out.print("\n");
		System.out.printf("%-5s = %-19s = %-4d\n", "0!", "", 1);
		System.out.printf("%-5s = %-19s = %-4d\n", "1!", "1", 1);
		System.out.printf("%-5s = %-19s = %-4d\n", "2!", "1 * 2", 4);
		System.out.printf("%-5s = %-19s = %-4d\n", "3!", "1 * 2 * 3", 6);
		System.out.printf("%-5s = %-19s = %-4d\n", "4!", "1 * 2 * 3 * 4", 24);
		System.out.printf("%-5s = %-19s = %-4d\n", "5!", "1 * 2 * 3 * 4 * 5", 120);
		System.out.print("\n \n");
		
		
		//Aufgabe3
		System.out.printf("\nAufgabe 3\n");
		
		String farenheit = "Fahrenheit";
		String celsius = "Celsius";
		System.out.printf("\n%-12s|%10s\n",farenheit, celsius);
		System.out.print("-----------------------");
		System.out.printf("\n%-12d|%10.2f",-20,-28.8889);
		System.out.printf("\n%-12d|%10.2f",-10,-23.3333);
		System.out.printf("\n%+-12d|%10.2f",-0,-17.7778);
		System.out.printf("\n%+-12d|%10.2f",20,-6.6667);
		System.out.printf("\n%+-12d|%10.2f\n",30,-1.1111);
	}
}
