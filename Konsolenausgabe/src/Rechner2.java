import java.util.Scanner; // Import der Klasse Scanner
public class Rechner2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		System.out.print("was ist dein Name? ");
		String Name = myScanner.next();
		
		System.out.print("wie alt bist du? ");
		int Alter = myScanner.nextInt();
		
		System.out.println("Dein Name ist " + Name + ". Du bist " + Alter + " Jahre alt.");
		myScanner.close();
	}

}
