﻿import java.util.Scanner;

class Fahrkartenautomat
{
	static Scanner tastatur = new Scanner(System.in);

	
    public static void main(String[] args)
    {
    	
    	double zuZahlenderBetrag;
       	double rückgabebetrag;
       	while(true) {
       		zuZahlenderBetrag = fahrkartenbestellungErfassen();
            
           	// Geldeinwurf
           	// -----------
           	rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

           	// Fahrscheinausgabe
           	// -----------------
           	fahrkartenAusgeben();

           	// Rückgeldberechnung und -Ausgabe
           	// -------------------------------
           	rueckgeldAusgeben(rückgabebetrag);
           	
           	System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                    "vor Fahrtantritt entwerten zu lassen!\n"+
                    "Wir wünschen Ihnen eine gute Fahrt.\n"
                    + "============================================================="
                    + "\n\n\n\n");
           	warte(1000);
       	}
       	
       
    }
    
    public static double fahrkartenbestellungErfassen() {
    	System.out.print("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n"
    			+ "Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
    			+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n\n"
    			+ "Ihre wahl:");
        int ticketWahl = tastatur.nextInt();
        
        double ticketPreis=1;
        boolean isNotValid=true;
        
        while(isNotValid) {
            switch(ticketWahl) {
            case 1:
            	ticketPreis = 2.90;
            	isNotValid = false;
            	break;
            case 2:
            	ticketPreis = 8.60;
            	isNotValid = false;
            	break;
            case 3:
            	ticketPreis = 23.50;
            	isNotValid = false;
            	break;
            }
            
            if (isNotValid) {
                System.out.print(">>falsche Eingabe<<\nIhre wahl:");
                ticketWahl = tastatur.nextInt();
            }
        }
  
        System.out.print("Anzahl der Tickets: ");
        int anzahlDerTickets = tastatur.nextInt();
        
        while(anzahlDerTickets < 1 || anzahlDerTickets > 10 ) {
        	System.out.println("die Anzahl der tickets sollte zwichen 1 und 10 sein.");
        	System.out.print("Anzahl der Tickets: ");
        	anzahlDerTickets = tastatur.nextInt();
        }
         
        return ticketPreis * anzahlDerTickets;
    }
    public static double fahrkartenBezahlen(double zuZahlen) {
        double eingezahlterGesamtbetrag = 0.0;

        while(eingezahlterGesamtbetrag < zuZahlen)
        {
     	   	System.out.printf("Noch zu zahlen: %.2f Euro ",(zuZahlen - eingezahlterGesamtbetrag));
     	   	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   	double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	return eingezahlterGesamtbetrag - zuZahlen;
    }
    public static void fahrkartenAusgeben() {// keine parameter weil es keine braucht
        System.out.println("\nFahrschein wird ausgegeben");

        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
       	{
       		System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
       		System.out.println("wird in folgenden Münzen ausgezahlt:");

       		while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
       		{
       			muezeAusgaben(2,"EURO");
       			rückgabebetrag -= 2.0;
       		}
       		while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
       		{
       			muezeAusgaben(1,"EURO");
       			rückgabebetrag -= 1.0;
       		}
       		while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
       		{
       			muezeAusgaben(50,"CENT");
       			rückgabebetrag -= 0.5;
       		}
       		while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
       		{
       			muezeAusgaben(20,"CENT");
       			rückgabebetrag -= 0.2;
       		}
       		while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
       		{
       			muezeAusgaben(10,"CENT");
       			rückgabebetrag -= 0.1;
       		}
   				while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
       			{
   				muezeAusgaben(5,"CENT");
       			rückgabebetrag -= 0.05;
       			}
       	}
    }
    
    public static void warte(int millisekunde) {
        try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
			}
    }
    public static void muezeAusgaben(int Betrag,String einheit) {
    	
    	System.out.printf("%2d %s\n",Betrag,einheit);
    }

}
