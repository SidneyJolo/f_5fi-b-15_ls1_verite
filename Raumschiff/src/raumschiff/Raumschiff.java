package raumschiff;

import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	
	static ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;

	
	public Raumschiff() {
		ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	public Raumschiff(int photonentorpedoAnzahl,
			int energieversorgungInProzent,
			int zustandSchildeInProzent,
			int zustandHuelleInProzent,
			int zustandLebenserhaltungssystemeInProzent,
			String schiffsname,
			int anzahlDroiden
			) {
		
		broadcastKommunikator = new ArrayList<String>();
		ladungsverzeichnis = new ArrayList<Ladung>();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.androidenAnzahl = anzahlDroiden;
		this.schiffsname = schiffsname;
	}
	
	public void addLadung(Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}
	
	static ArrayList<String> eintraegeLogbuchZurueckgeben(){
		return broadcastKommunikator;
	}
	
	public void zustandLadungsverzeichnis() {
		ArrayList<Ladung> arrayList = this.ladungsverzeichnis;
		System.out.printf("Ladungsverzeichnis von %s:\n",this.schiffsname);
		for(int i = 0;i<this.ladungsverzeichnis.size();i++) {
			Ladung currentLadung = (Ladung) arrayList.get(i);
			System.out.printf("%s: %d\n",currentLadung.getBezeichnung(),currentLadung.getMenge());
		}
		System.out.println();
	}
	
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(this.photonentorpedoAnzahl < 1) {
			this.click();
		} else {
			this.photonentorpedoAnzahl -= 1;
			this.nachrichtAnAlle("Photonentorpedo abgeschossen");
			r.treffer(r);
		}
	}
	
	public void photonentorpedosLaden(int anzahlTorpedosZuLaden) {
		ArrayList ladungsverzeichnisLocal = this.ladungsverzeichnis;
		boolean hatPhotonentorpedos = false;
		Ladung Photonentorpedos = new Ladung();
		
		for(int i = 0;i<ladungsverzeichnisLocal.size();i++) {
			Ladung currentLadung = (Ladung) ladungsverzeichnisLocal.get(i);
			if(currentLadung.getBezeichnung() == "photonentorpedo") {
				hatPhotonentorpedos = true;
				Photonentorpedos = currentLadung;
			}
		}
		
		if (!hatPhotonentorpedos) {
			System.out.println("Keine Photonentorpedos gefunden!\n");
			this.click();
		} else if (anzahlTorpedosZuLaden >= Photonentorpedos.getMenge()){
			this.photonentorpedoAnzahl += Photonentorpedos.getMenge();
			System.out.printf("%d Photonentorpedo(s) eingesetzt\n",Photonentorpedos.getMenge());
			Photonentorpedos.setMenge(0);
			ladungsverzeichnisLocal.remove(Photonentorpedos);
		} else {
			this.photonentorpedoAnzahl += anzahlTorpedosZuLaden;
			Photonentorpedos.setMenge(Photonentorpedos.getMenge()-anzahlTorpedosZuLaden);
			System.out.printf("%d Photonentorpedo(s) eingesetzt\n",anzahlTorpedosZuLaden);
			if (Photonentorpedos.getMenge() <= 0) {
				ladungsverzeichnisLocal.remove(Photonentorpedos);
			}
		}
		


	}
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if(this.energieversorgungInProzent<=50) {
			this.click();
		} else {
			this.energieversorgungInProzent -= this.haelfteVonIntAbgerundet(this.energieversorgungInProzent);
			this.nachrichtAnAlle("Phaserkanone abgeschossen");
			r.treffer(r);
		}
	}

	public void nachrichtAnAlle(String nachicht) {
		System.out.printf(nachicht);
		System.out.println();
		broadcastKommunikator.add(nachicht);
	}
	
	private void treffer(Raumschiff r) {
		this.nachrichtAnAlle(this.schiffsname + " wurde getroffen!");
		if(this.schildeInProzent > 0) {
			this.schildeInProzent = this.haelfteVonIntAbgerundet(this.schildeInProzent);
			
		} else if (this.huelleInProzent > 0) {
			this.huelleInProzent = this.haelfteVonIntAbgerundet(this.huelleInProzent);
		} else {
			this.lebenserhaltungssystemeInProzent = 0;
			this.nachrichtAnAlle(this.schiffsname + ": die Lebenserhaltungssysteme wurden vollst�ndig zerst�rt");
		}
	}
	
	public void reparaturDurchfuehren(
			Boolean schutzschilde,
			Boolean energieversorgung,
			Boolean schiffshuelle,
			int anzahlDroiden)
	{
		Random rand = new Random();
		
		int randNum = rand.nextInt(100);
		
		if (anzahlDroiden > this.androidenAnzahl) {
			anzahlDroiden = this.androidenAnzahl;
		}
		
		int anzahlSchiffstrukturen = 0;
		if (schutzschilde) { anzahlSchiffstrukturen++; }
		if (energieversorgung) { anzahlSchiffstrukturen++; }
		if (schiffshuelle) { anzahlSchiffstrukturen++; }
		
		int reparaturMenge = (randNum* Math.abs(anzahlDroiden))/(anzahlSchiffstrukturen);
		
		if (schutzschilde) { this.schildeInProzent += reparaturMenge; }
		if (energieversorgung) { this.energieversorgungInProzent += reparaturMenge; }
		if (schiffshuelle) { this.huelleInProzent += reparaturMenge; }
		
		
		
	}
	
	public void zustandRaumschiff() {
		System.out.println();
		System.out.print("------------------------------------------\n");
		System.out.printf("Name: %s\n", this.schiffsname);
		System.out.printf("Schild: %d %s\n", this.schildeInProzent,"%");
		System.out.printf("H�lle: %d %s\n", this.huelleInProzent,"%");
		System.out.printf("Energie: %d %s\n",this.energieversorgungInProzent,"%");
		System.out.printf("Lebenserhalungssysteme: %d %s\n",this.lebenserhaltungssystemeInProzent,"%");
		System.out.printf("Photonentorpedo Anzahl: %d\n", this.photonentorpedoAnzahl);
		System.out.printf("Androiden Anzahl %d \n", this.androidenAnzahl);
		System.out.print("------------------------------------------\n");
		System.out.println();
	}
	
	private void click() {
		this.nachrichtAnAlle(this.schiffsname + ": -=*click*=-");
	}
	
	private int haelfteVonIntAbgerundet(int intNummer) {
		return (int)Math.round((double)(intNummer/2));
	}
	
	
	//setter und getter
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}	
}
