package raumschiff;
import java.util.Random;
public class TestRaumschiff {
	
	public static void main(String[] args) {
		//situation Einrichten f�r 3 sterne
		
		Raumschiff romulaner = new Raumschiff(2,100,100,100,100,"IRW Khazara",2);
		Ladung borg_schrott = new Ladung("borg_schrott",5);
		Ladung rote_Marterie = new Ladung("Rote Materie",2);
		Ladung plasma_waffe = new Ladung("Plasma-waffe",50);
		romulaner.addLadung(borg_schrott);
		romulaner.addLadung(rote_Marterie);
		romulaner.addLadung(plasma_waffe);
		
		Raumschiff vulkanier = new Raumschiff(0,80,80,50,100,"Ni'Var",5);
		Ladung forschungssonde = new Ladung("forschungssonde",35);
		Ladung Photonentorpedos = new Ladung("photonentorpedo",3);
		vulkanier.addLadung(forschungssonde);
		vulkanier.addLadung(Photonentorpedos);
		
		Raumschiff klingonen = new Raumschiff(1,100,100,100,100,"IKS Hegh'ta",2);		
		Ladung ferengi_schneckensaft = new Ladung("Ferengi Schneckensaft",200);
		Ladung batLeth_klingonen_schwert = new Ladung("bat'leth Slingonen Schwert",200);
		klingonen.addLadung(batLeth_klingonen_schwert);
		klingonen.addLadung(ferengi_schneckensaft);
		
		//tests 
		klingonen.photonentorpedoSchiessen(romulaner);
		
		romulaner.phaserkanoneSchiessen(klingonen);
		
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		
		klingonen.zustandRaumschiff();
		klingonen.zustandLadungsverzeichnis();
		
		vulkanier.reparaturDurchfuehren(true, false, true, 99);
		
		vulkanier.photonentorpedosLaden(4);
		
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		
		klingonen.zustandRaumschiff();
		klingonen.zustandLadungsverzeichnis();
		vulkanier.zustandRaumschiff();
		vulkanier.zustandLadungsverzeichnis();
		romulaner.zustandRaumschiff();
		romulaner.zustandLadungsverzeichnis();
		
		System.out.print(Raumschiff.eintraegeLogbuchZurueckgeben());
		System.out.print("");
		
	}
}
