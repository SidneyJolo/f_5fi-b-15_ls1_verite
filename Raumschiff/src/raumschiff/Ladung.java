package raumschiff;

public class Ladung {
	private String bezeichnung;
	private int menge;
	
	public Ladung() {
	}
	
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	/*
	 * @Override
	 */
	public String toString() {
		String output = this.bezeichnung + ": " + this.menge;
		return output;

	}
	
	
	//getter und setter
	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
}
