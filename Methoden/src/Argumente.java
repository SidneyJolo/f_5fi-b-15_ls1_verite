public class Argumente {
	public static void main(String[] args) {
		ausgabe(1, "Mana");
		ausgabe(2, "Elise");
		ausgabe(3, "Johanna");
		ausgabe(4, "Felizitas");
		ausgabe(5, "Karla");
		System.out.println(vergleichen(1, 2));
		System.out.println(vergleichen(1, 5));
		System.out.println(vergleichen(3, 4));
	}
	public static void ausgabe(int zahl, String name) {
		System.out.println(zahl + ": " + name);
	}
	public static boolean vergleichen(int arg1, int arg2) {
		return (arg1 + 8) < (arg2 * 3);
	}
}

//alles sollte ohne Fehler functionieren:
//	1: Mana
//	2: Elise
//	3: Johanna
//  4: Felizitas
// 	5: Karla
//	false
//  true
//	true