import java.util.Scanner;

public class MultiplicationMethodenubungAufgabe2 {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("numer 1: ");
		double num1 = myScanner.nextDouble();
		System.out.print("numer 2: ");
		double num2 = myScanner.nextDouble();
		myScanner.close();
		double ergebnisse = multiplication(num1,num2);
		System.out.printf("%f",ergebnisse);
	}
	public static double multiplication(double num1, double num2) {
		return num1*num2;
	}

}
