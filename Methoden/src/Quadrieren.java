import java.util.Scanner;
public class Quadrieren {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert für x festlegen:
		// ===========================
		titel();
		Scanner myScanner = new Scanner(System.in);
		System.out.println("gebe eine Zahl ein: ");

		double x = myScanner.nextDouble();
				
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis = quadrieren(x);
		
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		Ausgabe(x,ergebnis);
	}
	
	public static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x^2");
		System.out.println("---------------------------------------------");
	}
	
	public static double quadrieren(double x) {
		return x*x;
	}
	
	public static void Ausgabe(double x, double ergebnis) {
		System.out.printf("x = %.2f und x^2= %.2f\n", x, ergebnis);
	}
	
}
