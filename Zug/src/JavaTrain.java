import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int fahrzeit = 0;
		
		char haltInSpandau;
		System.out.println("soll der Zug in Spandau anhalten(j/n)?");
		haltInSpandau = myScanner.next().charAt(0);
		
		char richtungHamburg;
		System.out.println("soll der Zug nach Hamburg fahren?(j/n)?");
		richtungHamburg = myScanner.next().charAt(0);
		
		char haltInStendal = 'n';
		char endetIn = 'z'; 
		if (richtungHamburg == 'n') {
			System.out.println("soll der Zug durch Stendal fahren(j/n)?");
			haltInSpandau = myScanner.next().charAt(0);
			
			System.out.println("wo soll der zug enden(h/w/b)?");
			endetIn = myScanner.next().charAt(0);
		}
		

		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau
			
		if (haltInSpandau == 'j') {
			fahrzeit = fahrzeit + 2; // Halt in Spandau
		}
		if (richtungHamburg == 'j') {
			fahrzeit = fahrzeit + 96; // geht direckt nach Hamburg
			System.out.printf("Sie erreichen Hamburg nach %d minuten",fahrzeit);
		} else {
			fahrzeit = fahrzeit + 34; // die fahrt von spandau nach Stendal ost
			if (haltInStendal == 'j') {
				fahrzeit = fahrzeit + 16; // halt in Stendal
			} else {
				fahrzeit = fahrzeit + 6; // Umfahrung von stendal
			}
			if (endetIn == 'w') {
				fahrzeit = fahrzeit + 29; // zug endet in Wolfsburg	
				System.out.printf("Sie erreichen Wolfsburg nach %d minuten",fahrzeit);
				
			} else {
				if (endetIn == 'h') {
					fahrzeit = fahrzeit + 62; // zug endet in Hannover
					System.out.printf("Sie erreichen Hannover nach %d minuten",fahrzeit);
				}
				if (endetIn == 'b'){
					fahrzeit = fahrzeit + 50; // zug endet in Brandschweig
					System.out.printf("Sie erreichen Brandschweig nach %d minuten",fahrzeit);
				}
			}
		}
	}
}
