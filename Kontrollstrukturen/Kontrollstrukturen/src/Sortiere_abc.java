import java.util.Scanner;
public class Sortiere_abc {
	//A4.1 Auswahlstrukturen  Aufgabe 7: Sortieren	
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String zeichenkette;
		System.out.println("bitte geben sie ein dreistellige zeichenkette an?");
		zeichenkette = myScanner.next();
		myScanner.close();
		
		for(int i=0; i<3; i++) {
		zeichenkette = reorganisirungsCyclus(zeichenkette);
		zeichenkette = reorganisirungsCyclus(zeichenkette);
		zeichenkette = reorganisirungsCyclus(zeichenkette);
		}
		System.out.println(zeichenkette);
		
	}
	
	public static String reorganisirungsCyclus(String zeichenkette) {//bad bubble sorting
		String tempzeichenkette = zeichenkette;
		
		if((int)tempzeichenkette.charAt(0) > (int)tempzeichenkette.charAt(1)) {  //typ castin a char to a int gives the ascii value
			tempzeichenkette = Character.toString(tempzeichenkette.charAt(1))
					+ Character.toString(tempzeichenkette.charAt(0))
					+ Character.toString(tempzeichenkette.charAt(2));
		}	// (abc) » if a > b » (bac)
		
		if((int)tempzeichenkette.charAt(1) > (int)tempzeichenkette.charAt(2)) { 
			tempzeichenkette = Character.toString(tempzeichenkette.charAt(0))
					+ Character.toString(tempzeichenkette.charAt(2))
					+ Character.toString(tempzeichenkette.charAt(1));
		}	// (bac) » if b > c » (bca)
		
		return tempzeichenkette;
	}
}
