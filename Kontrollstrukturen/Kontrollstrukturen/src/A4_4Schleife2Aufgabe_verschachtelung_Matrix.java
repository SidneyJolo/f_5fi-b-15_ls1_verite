import java.util.Scanner;
public class A4_4Schleife2Aufgabe_verschachtelung_Matrix {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben sie eine Zahl zwichen 2 un 9 ein:");
		int eingabeZahl = myScanner.nextInt();
		int iteraorZehn = 0;
		int iteratorEins = 0;
		
		while(iteraorZehn<10) {
			iteratorEins = 0;
			while(iteratorEins<10) {
				int wert = ((iteraorZehn*10)+iteratorEins) ;
				if(wert % eingabeZahl == 0 || istZahlInNummer(wert,eingabeZahl) || istQuersummeGleichZahl(wert,eingabeZahl)) {
					System.out.print("  *  ");
					//System.out.printf("%5d",wert);

				} else {
					System.out.printf("%5d",wert);
				}
				iteratorEins++;
				
			}
			iteraorZehn++;
			System.out.print("\n");
		}
		
	}
	
	public static boolean istZahlInNummer(int nummer,int zahl) {
		
		String string_number = String.valueOf(nummer);
		char char_zahl = String.valueOf(zahl).charAt(0);
		for(int i = 0;i<string_number.length();i++) {
			if(string_number.charAt(i) == char_zahl){
				return true;
			}
		}
		
		return false;
	}
	
	public static boolean istQuersummeGleichZahl(int nummer,int zahl) {
		String string_number = String.valueOf(nummer);
		int summe = 0;
		for(int i = 0;i<string_number.length();i++) {
			summe += Character.getNumericValue(string_number.charAt(i));
			}
		if(summe == zahl) {
			return true;
		}
		
		return false;
	}

}
