import java.util.Scanner;
public class A4_1AuswahlstrukturenAufgabe4Rabattsystem {
	
	public static void main(String[] args) 
	{
		Scanner myScanner = new Scanner(System.in);
		
		double bestellwert;
		double ermaesigteBestellwert = 0;
		System.out.println("was ist der Bestellwert");
		bestellwert = myScanner.nextDouble();
		
		double MwSt = 0.19; 
		
		if(bestellwert <= 100) {
			
			ermaesigteBestellwert = bestellwert-prozentZahlRechnung(bestellwert,10);
		}
		if(bestellwert <= 500 && bestellwert > 100) {
			
			ermaesigteBestellwert = bestellwert-prozentZahlRechnung(bestellwert,15);
		}
		if(bestellwert > 500) {
			
			ermaesigteBestellwert = bestellwert-prozentZahlRechnung(bestellwert,20);
		}
		
		ermaesigteBestellwert += prozentZahlRechnung(ermaesigteBestellwert,19);
		
		System.out.printf("\n%.2f Euro",ermaesigteBestellwert);
	}
	
	public static double prozentZahlRechnung(double betrag,double prozentteil) {
		
		double erg = ((betrag/100)*prozentteil);
		
		return erg;
	}

}
