import java.util.Scanner;
public class A4_1AuswahlstrukturenAufgabe8Schaltjahr {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int jahr;
		System.out.println("bitte Jahr angeben.");
		jahr = myScanner.nextInt();
		boolean istEinSchaltjahr;
		
		myScanner.close();
		
		
		istEinSchaltjahr = false;
		
		if(jahr < -45) { // vor 45 v. Chr
			istEinSchaltjahr = false;
			
		} else if(jahr < 1582) { // vor 1582
			
			if(jahr % 4 == 0) {// nur regel 1
				istEinSchaltjahr = true;
			}
			
		} else { // nach 1582
			
			if(jahr % 4 == 0) {// regel 1
				istEinSchaltjahr = true;
			}
			
				if(jahr % 100 == 0) { // regel 2
					istEinSchaltjahr = false;
				}
			
					if(jahr % 400 == 0) { // regel 3
						istEinSchaltjahr = true;
					}
		}
		
		
		if(!istEinSchaltjahr) {
			System.out.printf("%d ist KEIN Schaltjahr",jahr);
		} else {
			System.out.printf("%d ist ein Schaltjahr",jahr);
		}
	}
}
