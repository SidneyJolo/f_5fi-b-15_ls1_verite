import java.util.Scanner;
public class A4_1_Fallunterscheidung_Aufgabe2Monate {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		int monat;
		System.out.println("welcher monat(nummer)?");
		monat = myScanner.nextInt();
		
		String monatString;
        
		myScanner.close();
        
		
        switch (monat) {
            case 1:  monatString = "Januar";
                     break;
            case 2:  monatString = "Februar";
                     break;
            case 3:  monatString = "März";
                     break;
            case 4:  monatString = "April";
                     break;
            case 5:  monatString = "Mai";
                     break;
            case 6:  monatString = "Juni";
                     break;
            case 7:  monatString = "Juli";
                     break;
            case 8:  monatString = "August";
                     break;
            case 9:  monatString = "September";
                     break;
            case 10: monatString = "Oktober";
                     break;
            case 11: monatString = "November";
                     break;
            case 12: monatString = "Dezember";
                     break;
            default: monatString = "invalider Monat";
                     break;
        }
        
        
        System.out.printf("der %d. monat des Jahres ist %s ",monat,monatString);
	}

}
