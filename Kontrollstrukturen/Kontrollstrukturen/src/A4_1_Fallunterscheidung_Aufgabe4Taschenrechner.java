import java.util.Scanner;
public class A4_1_Fallunterscheidung_Aufgabe4Taschenrechner {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		double ersteNummer;
		System.out.println("bitte erste numern eingeben");
		ersteNummer = myScanner.nextDouble();
		
		double zweiteNummer;
		System.out.println("bitte zweite numern eingeben");
		zweiteNummer = myScanner.nextDouble();
		
		char operator;
		System.out.println("bitte operator eingeben(+-*/)");
		operator = myScanner.next().charAt(0);
		
		double ergebnis = 0;
		boolean istOperatorValide = false;
		
		myScanner.close();
		
		
		switch(operator) {
		case '+':
			ergebnis = ersteNummer + zweiteNummer;
			istOperatorValide = true;
			break;
		case '-':
			ergebnis = ersteNummer - zweiteNummer;
			istOperatorValide = true;
			break;
		case '*':
			ergebnis = ersteNummer * zweiteNummer;
			istOperatorValide = true;
			break;
		case '/':
			ergebnis = ersteNummer / zweiteNummer;
			istOperatorValide = true;
			break;
		default:
			istOperatorValide = false;
			break;
		}
		
		
		if(istOperatorValide) {
			System.out.printf("%.2f %s %.2f = %.2f",ersteNummer,operator,zweiteNummer,ergebnis);
		} else {
			System.out.println("invalider operator");
		}
		
	}

}
