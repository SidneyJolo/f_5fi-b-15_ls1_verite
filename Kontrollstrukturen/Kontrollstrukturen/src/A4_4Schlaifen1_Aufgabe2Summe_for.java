import java.util.Scanner;
public class A4_4Schlaifen1_Aufgabe2Summe_for {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		int maxIterator;
		System.out.println("geben sie bitte einen begrenzenden Wert ein: ");
		maxIterator = myScanner.nextInt();
		
		int summeA = 0;
		int summeB = 0;
		int summeC = 0;
		
		
		for(int i = 0;i <= maxIterator;i++) {
			summeA += i;
			summeB += i*2;
			summeC += (i*2)+1;
		}
		
		System.out.printf("Die Summe f�r A betr�gt: %d\n",summeA);
		System.out.printf("Die Summe f�r B betr�gt: %d\n",summeB);
		System.out.printf("Die Summe f�r C betr�gt: %d\n",summeC);
		
	}

}
