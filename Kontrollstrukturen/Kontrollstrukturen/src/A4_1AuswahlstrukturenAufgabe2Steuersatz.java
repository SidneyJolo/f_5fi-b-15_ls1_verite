import java.util.Scanner;
public class A4_1AuswahlstrukturenAufgabe2Steuersatz {

	public static void main(String[] args) {
		// Scanner
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben die ihren Bestellwert ein");
		double bestellwert = myScanner.nextDouble();
		
		double mwst =  0.19;
		
	if (bestellwert <= 100){
		bestellwert = bestellwert - bestellwert* 0.10;
		bestellwert = bestellwert + bestellwert * mwst;
		System.out.printf("Sie erhalten einen 10 prozentigen Rabatt. Ihre Bestellung(inkl. MwSt): %.2f", bestellwert);
	} else if (bestellwert <= 500){
		bestellwert = bestellwert - bestellwert* 0.15;
		bestellwert = bestellwert + bestellwert * mwst;
		System.out.printf("Sie erhalten einen 15 prozentigen Rabatt. Ihre Bestellung(inkl. MwSt): %.2f", bestellwert);
	} else if (bestellwert > 500){
		bestellwert = bestellwert - bestellwert* 0.20;
		bestellwert = bestellwert + bestellwert * mwst;
		System.out.printf("Sie erhalten einen 10 prozentigen Rabatt. Ihre Bestellung(inkl. MwSt): %.2f", bestellwert); 
		
	} else {
		System.out.println("Fehler");
		}
	}

}

