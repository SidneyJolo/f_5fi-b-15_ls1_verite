import java.util.Scanner;
public class A4_1AuswahlstrukturenAufgabe6Funktionslöser {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		double x;
		System.out.print("x = ");
		x = myScanner.nextDouble();
		
		double funktionswert = 0;
		String bereich = "error";
		final double e = 2.718;
		
		
		if(x <= 0) {
			funktionswert = Math.pow(e, x);
			bereich = "exponentiell";
		} else if(x <= 3) {
			funktionswert = Math.pow(x, 2) + 1;
			bereich = "quadratisch";
		} else if(x > 3) {
			funktionswert = 2*x + 4;
			bereich = "linear";
		}
		
		
		System.out.printf("(%f)f = %f , x befindet sich im bereich %s",x,funktionswert,bereich);
	}

}
