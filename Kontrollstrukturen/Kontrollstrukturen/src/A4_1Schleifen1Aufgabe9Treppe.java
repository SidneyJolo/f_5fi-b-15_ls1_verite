import java.util.Scanner;
public class A4_1Schleifen1Aufgabe9Treppe {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Treppenhöhe: ");
		int Treppenhöhe = myScanner.nextInt();
		System.out.print("Stufenbreite: ");
		int Stufenbreite = myScanner.nextInt();
		
		
		for(int i = 1;i<=Treppenhöhe;i++) {
			for(int k = 1;k <= (Treppenhöhe-i)*Stufenbreite;k++) {
				System.out.print(" ");
			}
			for(int j = 1;j <= Stufenbreite*i;j++) {
				System.out.print("*");
			}
			System.out.print("\n");
		}
	}

}


