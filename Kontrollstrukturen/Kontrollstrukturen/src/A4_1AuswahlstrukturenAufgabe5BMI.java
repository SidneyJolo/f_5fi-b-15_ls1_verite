import java.util.Scanner;
public class A4_1AuswahlstrukturenAufgabe5BMI {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		double gewicht;
		System.out.println("was ist ihr Gewicht(Kg)");
		gewicht = myScanner.nextDouble();
		double groesse;
		System.out.println("was ist ihr Größe(cm)");
		groesse = myScanner.nextDouble();
		char geschlecht;
		System.out.println("was ist ihr Geschlecht(m/w)");
		geschlecht = myScanner.next().charAt(0);
		
		double bmi;
		String bmiKlassifikation = "error";
		
		
		bmi = gewicht/((groesse/100)*(groesse/100));
		
		if(geschlecht == 'm') {
			if(bmi<20) {
				bmiKlassifikation = "Untergewicht";
			} else if(bmi <=25) {
				bmiKlassifikation = "Normalgewicht";
			} else if(bmi >25) {
				bmiKlassifikation = "Übergewicht";
			}
		}
		
		if(geschlecht == 'w') {
			if(bmi<19) {
				bmiKlassifikation = "Untergewicht";
			} else if(bmi <=24) { 
				bmiKlassifikation = "Normalgewicht";
			} else if(bmi >24) {
				bmiKlassifikation = "Übergewicht";
			}
		}
		
		System.out.printf("mit einem BMI von %.1f ist deine BMI-Klassifikation: %s ",bmi,bmiKlassifikation);
		
	}

}
