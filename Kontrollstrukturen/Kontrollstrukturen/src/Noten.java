import java.util.Scanner;
public class Noten {
	//A4.1 "AB Fallunterscheidung" Aufgabe 1: Noten
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		int note;
		System.out.println("was ist die note? ");
		note = myScanner.nextInt();
		String ausgabe;
		
		myScanner.close();
		
		switch(note) {
		case 1:
			ausgabe = "Sehr gut";
			break;
		case 2:
			ausgabe = "Gut";
			break;
		case 3:
			ausgabe = "Befriedigend";
			break;
		case 4:
			ausgabe = "Ausreichend";
			break;
		case 5:
			ausgabe = "Mangelhaft";
			break;
		case 6:
			ausgabe = "Ungenügend";
			break;
		default:
			ausgabe = "Eingabe Fehler";
			
		
		}
		System.out.printf("mit eine %d gillt als %s", note,ausgabe);
		
	}

}
