import java.util.Scanner;
public class A4_1_Fallunterscheidung_Aufgabe3RomischeZahlen {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		char roemicheZahl;
		System.out.println("welcher römiche Zahl (I/V/X/L/C/D/M)(Großgeschrieben)?");
		roemicheZahl = myScanner.next().charAt(0);
		
		int dezimalZahl;
        
		myScanner.close();
        
		
        switch (roemicheZahl) {
            case 'I':  dezimalZahl = 1;
                     break;
            case 'V':  dezimalZahl = 5;
            	break;
            case 'X':  dezimalZahl = 10;
        		break;
            case 'L':  dezimalZahl = 50;
        		break;
            case 'C':  dezimalZahl = 100;
        		break;
            case 'D':  dezimalZahl = 500;
        		break;
            case 'M':  dezimalZahl = 1000;
        		break;
            default: dezimalZahl = 404;
                     break;
        }
        
        
        if(dezimalZahl != 404) {
            System.out.printf("%s ist gleich %d in decimal",roemicheZahl,dezimalZahl);
        } else {
        	System.out.println("die Eingabe war falch, versuchen sie eine von disen (I/V/X/L/C/D/M)");
        }
        
	}

}