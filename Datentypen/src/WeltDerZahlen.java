/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

	public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 8 ;
    
    // Anzahl der Sterne in unserer Milchstraße
    long anzahlSterne = 100000000;
    
    // Wie viele Einwohner hat Berlin?
    long bewohnerBerlin = 3600000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 7675;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm =   150000;
    
    // Schreiben Sie auf, wie viele km² das größter Land der Erde hat?
    long flaecheGroessteLand = 17090246;
    
    // Wie groß ist das kleinste Land der Erde?
    
    float flaecheKleinsteLand = 0.44f; 
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("bewohnerBerlin: " + bewohnerBerlin);
    
    System.out.println("alterTage: " + alterTage);
        
    System.out.println("gewichtKilogramm: " + gewichtKilogramm);
    
    System.out.println("flaecheGroessteLand: " + flaecheGroessteLand);
        
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}