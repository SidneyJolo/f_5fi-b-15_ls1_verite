
public class DvD {
	
	private int artikelNummer;
	private double einkaufsPreis;
	private double verkaufsPreis;
	private String name;
	private int mindesbestand;
	private int lagerbestand;
	
	public DvD() {
	}
	
	public DvD(String name,int artikelNummer,double einkaufsPreis,double verkaufsPreis,int mindesbestand,int lagerbestand) {
		this.artikelNummer = artikelNummer;
		this.einkaufsPreis = einkaufsPreis;
		this.verkaufsPreis = verkaufsPreis;
		this.name = name;
		this.mindesbestand = mindesbestand;
		this.lagerbestand = lagerbestand;
	}
	
	public void bestellArtikel(int anzahl) {
		this.lagerbestand += anzahl;
	}
	
	public void verkaufArtikel(int anzahl) {
		if (anzahl > this.lagerbestand) {
			System.out.printf("you cannot buy %d %ss", anzahl,this.name);
		}
		this.lagerbestand -= anzahl;
		
		if (this.lagerbestand < mindesbestand*0.8) {
			this.bestellArtikel(10);
		}
	}
	
	public double berechneGewinnmarge() {
		return this.einkaufsPreis - this.verkaufsPreis;
	}
	
	//getter and setter
	public double getEinkaufsPreis() {
		return einkaufsPreis;
	}

	public void setEinkaufsPreis(double einkaufsPreis) {
		this.einkaufsPreis = einkaufsPreis;
	}

	public double getVerkaufsPreis() {
		return verkaufsPreis;
	}

	public void setVerkaufsPreis(double verkaufsPreis) {
		this.verkaufsPreis = verkaufsPreis;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMindesbestand() {
		return mindesbestand;
	}

	public void setMindesbestand(int mindesbestand) {
		this.mindesbestand = mindesbestand;
	}

	public int getLagerbestand() {
		return lagerbestand;
	}

	public void setLagerbestand(int lagerbestand) {
		this.lagerbestand = lagerbestand;
	}

	public int getArtikelNummer() {
		return artikelNummer;
	}

	public void setArtikelNummer(int artikelNummer) {
		this.artikelNummer = artikelNummer;
	}

}
