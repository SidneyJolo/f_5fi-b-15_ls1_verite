public class Lager {
	
	public static void main(String[] args) {
		
		DvD Himalayareis = new DvD("Himalayareis",12,1.41,1.78,100,412);
		DvD FrischeFischeVonFischersFritze = new DvD();

		FrischeFischeVonFischersFritze.setName("Frische Fische von Fischers Fritze");
		FrischeFischeVonFischersFritze.setArtikelNummer(581);
		FrischeFischeVonFischersFritze.setEinkaufsPreis(7.21);
		FrischeFischeVonFischersFritze.setVerkaufsPreis(12.45);
		FrischeFischeVonFischersFritze.setMindesbestand(10);
		FrischeFischeVonFischersFritze.setLagerbestand(3);
	}
}
