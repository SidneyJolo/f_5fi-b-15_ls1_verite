package test;

import java.util.Scanner;

public class KlausurUbung {

	public static void main(String[] args) {
		// �bung 1
		int i = 10;
		int zahl = 5;

		while (i > 3){
			i--;
			switch (i) {
			case 1:
				zahl += 1;
				break;
			case 2:
				zahl += 2;
				break;
			case 3:
				zahl += 3;
				break;
			default:
			}
		} 
		if (zahl % 10 != 0)
			zahl = zahl + 100;
		System.out.println(i + "" + zahl);

		// �bung 2
		int ergebnis = 5;
		for(int j = 4; j <8; j++) {
			if (j > 6)
				ergebnis--;
			else
				ergebnis = j + 2;
			ergebnis = ergebnis + 5;
		}
		++ergebnis;
		System.out.println(ergebnis);

		// �bung 3
		int k = 3;
		int zahl1 = 5;
		do {
			switch (k) {
			case 1:
				zahl1 = --zahl1 + 15;
				break;
			case 2:
				zahl1 = zahl1-- + 5;
				break;
			case 3:
			case 4:
				zahl1 = --zahl1 + 10;
				break;
			default:
			}
			k++;
		} while (k > 10);
		System.out.println(k + "" + zahl1);
		
		// �bung 4 ARRAYS
		int[] zahlen = new int[5];
		zahlen[0] = 3;
		zahlen[1] = 6;
		zahlen[2] = 9;
		zahlen[3] = 12;
		zahlen[4] = 15;

		int[] feld = { 2, 3, 5, 7, 11 };
		
		for(int m = 0; m < feld.length; m++)
			System.out.print("feld["+ m +"]: "+ feld[m] + "  ");

		
	}

}
